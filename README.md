Hacker News Jobs
================

**A React application which uses Hacker News as the data source.**

1. Select a single "ASK HN: Who is hiring" thread, using your preferred data scraping method (e.g. puppeteer) or by using their API.
2. Use the data gathered from that thread to build an app that allows to display job offers
3. Create a filter for those listings based on position/salary/remote

# Build instructions

## Install dependencies
Run:
```
npm install --save
```

## Build assets
Run:
```
# development
npm run dev

# production
npm run build
```

## Start Webpack Dev Server
Run:
```
npm run start

# or
./node_modules/.bin/webpack-dev-server --mode development --open
```
The default address is http://localhost:8080

# Deploy to Docker
Run commands:
```
npm run docker-build
npm run docker-run

# or just
docker build -t krnr/hackernews:latest .
docker run -p 8080:80 -td krnr/hackernews
```
The default address is http://localhost:8080
