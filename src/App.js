import React from "react";
import ReactDOM from "react-dom";
import HackerNews from "./components/HackerNews"

const App = () => {
  return (
    <HackerNews />
  );
};
export default App;
ReactDOM.render(<App />, document.getElementById("app"));