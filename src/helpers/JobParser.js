const trends = [
    '.net', 
    'angular', 
    'apollo', 
    'aws', 
    'bash', 
    'blockchain', 
    'c++', 
    'c#', 
    'cassandra', 
    'clojurescript', 
    'contract', 
    'd3', 
    'deep learning', 
    'devops', 
    'django', 
    'docker', 
    'elasticsearch', 
    'elixir', 
    'elm', 
    'ember', 
    'flux', 
    'foundationdb', 
    'freelance', 
    'functional', 
    'golang', 
    'google cloud', 
    'graphql', 
    'haskell', 
    'heroku', 
    'java ', 
    'javascript', 
    'jupyter', 
    'kafka', 
    'kubernetes', 
    'machine learning', 
    'mongodb', 
    'mysql', 
    'natural language', 
    'next.js', 
    'node', 
    'objective-c', 
    'ocaml', 
    'php', 
    'postgres', 
    'python', 
    'rails', 
    'react', 
    'redis', 
    'redux', 
    'ruby', 
    'scala', 
    'spark', 
    'swift', 
    'terraform', 
    'typescript', 
    'vue'
]

const JobParser = (jobdata) => {
    const lowercase = jobdata.text.toLowerCase()
    const findCompanyName = (jobstring) => {
        return jobstring.split('|')[0].trim()
    }

    const onsiteOrRemote = (jobstring) => {
        const officework = []
        if (jobstring.search(/on ?site/i) != -1) {
            officework.push("onsite")
        }
        if (jobstring.search(/remote/i) != -1) {
            officework.push("remote")
        }
        return officework
    }

    const findTrends = (jobstring) => {
        return trends.filter((trend) => { return jobstring.indexOf(trend) >= 0 } )
    }

    return {
        id: jobdata.id,
        company: findCompanyName(jobdata.text),
        trends: findTrends(lowercase),
        officework: onsiteOrRemote(lowercase),
        date: new Date(jobdata.time * 1000),
        url: "https://news.ycombinator.com/item?id=" + jobdata.id,
        text: jobdata.text
    }
}

export default JobParser;