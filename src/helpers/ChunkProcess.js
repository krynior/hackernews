const chunkArray = (arr, count) => {
    let chunked = []
    arr.forEach((element, index) => {
        const ind = Math.trunc(index / count)
        typeof chunked[ind] == 'undefined'
            ? chunked[ind] = [element]
            : chunked[ind].push(element)
    }, [])
    return chunked
}

const promiseSequential = (array, callback) => {
    return array.reduce((accumulator, element) => {
      return accumulator.then((arr) => {
        return new Promise((resolve, reject) => {
          callback(element).then((result) => resolve([...arr, result]), (result) => reject(result))
        })
      })
    }, Promise.resolve([]))
}


export {chunkArray, promiseSequential}