import React from "react"
import {Badge} from "reactstrap"

const Tag = (props) => {
    switch (props.kind) {
    case "remote":
        return (
            <Badge color="primary">REMOTE</Badge>
        )
    case "onsite":
        return (
            <Badge color="danger">ONSITE</Badge>
        )
    default:
        return ( 
            <Badge>{props.kind.toUpperCase()}</Badge>
        )
    }
}

export default Tag;