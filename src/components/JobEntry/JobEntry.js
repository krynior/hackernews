import React from "react"
import {Card, CardBody, CardText, CardHeader, CardFooter} from "reactstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLink } from '@fortawesome/free-solid-svg-icons'
import Tag from "./Tag";
import sanitizehtml from 'sanitize-html'

const JobEntry = (props) => {
    return (
        <Card outline className="jobentry">
        <CardHeader className="jobentry-header clearfix">
          <strong>{props.title}</strong>
          <span className="float-right">{props.date.toLocaleString()}</span>
        </CardHeader>
        <CardBody>
          <CardText><span dangerouslySetInnerHTML={{ __html: sanitizehtml(props.content)}} /></CardText>
        </CardBody>
        <CardFooter className="clearfix">
          <span className="float-left">
          {
            props.officework.map((work) => {
              return <Tag key={"work"+props.id+work} kind={work} />
            })
          }
          {
            props.trends.map((trend) => {
              return <Tag key={"trend"+props.id+trend} kind={trend} />
            })
          }
          </span>
          <span className="float-right">
            <a href={props.href} target="_blank"><FontAwesomeIcon icon={faLink} /> Hacker News link</a>
          </span>
        </CardFooter>
      </Card>
    )
}

export default JobEntry;