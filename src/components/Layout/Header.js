import React from 'react';
import {
  Container,
  Navbar,
  NavbarBrand } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faNewspaper } from '@fortawesome/free-solid-svg-icons'

export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar className="hn-navbar" dark expand="md">
          <Container>
          <NavbarBrand href="/"><FontAwesomeIcon icon={faNewspaper} /> Hacker News Jobs</NavbarBrand>
          </Container>
        </Navbar>
      </div>
    );
  }
}
