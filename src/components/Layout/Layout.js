import React from "react"
import { Container } from 'reactstrap';
import Header from "./Header";
import JobEntry from "../JobEntry/JobEntry";
import SearchForm from "../SearchForm/SearchForm"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSkull } from '@fortawesome/free-solid-svg-icons'

const Layout = (props) => {
    const filteredData = props.posts.filter((data) => {
        const srch = props.searchTextDefer.trim().toLowerCase()
        return (srch.length == 0 || data.text.toLowerCase().indexOf(srch) >= 0)
            && (props.selectedTrends.length == 0 || data.trends.reduce((prev, cur) => { return prev || props.selectedTrends.includes(cur) }, false))
            && (props.selectedLocation.length == 0 || data.officework.reduce((prev, cur) => { return prev || props.selectedLocation.includes(cur) }, false))
    })
    return (
        <div>
            <header>
                <Header>
                </Header>
            </header>
            <main>
            <Container>
                <SearchForm onSubmit={props.onSubmit}
                            onUrlChange={props.onUrlChange}
                            alert={props.alert}
                            onAlertDismiss={props.onAlertDismiss}
                            currentState={props.currentState}
                            progress={props.progress}
                            stats={{total: props.posts.length, filtered: filteredData.length}}
                            onExampleUrl={props.onExampleUrl}
                            url={props.url}
                            searchText={props.searchText}
                            searchTrends={props.searchTrends}
                            selectedTrends={props.selectedTrends}
                            onTrendSelected={props.onTrendSelected}
                            onSearchChange={props.onSearchChange}
                            selectedLocation={props.selectedLocation}
                            onLocationSelected={props.onLocationSelected}
                            onResetLocation={props.onResetLocation}
                            onResetTrends={props.onResetTrends} />
                {
                    filteredData.map((data) => {
                        return <JobEntry key={"job"+data.id} content={data.text} href={data.url} title={data.company} trends={data.trends} officework={data.officework} date={data.date} />
                    })
                }
            </Container>
            <footer style={{textAlign: 'center'}}>
                <p>2019 krnr - made with <FontAwesomeIcon icon={faSkull} /> in warsaw</p>
            </footer>
            </main>
        </div>
    );
}

export default Layout