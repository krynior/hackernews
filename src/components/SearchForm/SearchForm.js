import React from "react"
import { Alert, Button, ButtonGroup, Card, CardBody, CardFooter, Col, Form, FormGroup, Input, Progress, Spinner } from "reactstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'

const SearchForm = (props) => {
    return (
        <Card className="searchform">
            <CardBody>
                <Alert color="danger" isOpen={props.alert.show} toggle={props.onAlertDismiss}>
                    {props.alert.text}
                </Alert>
                <Form>
                    <FormGroup>
                        <label htmlFor="hnUrl">Hacker News Post URL</label>
                        <Input  type="text" name="hnUrl" id="hnUrl"
                                placeholder="e.g. https://news.ycombinator.com/item?id=18807017"
                                onChange={props.onUrlChange}
                                value={props.url}
                                disabled={props.currentState=="loading"} />
                    </FormGroup>
                    <Button disabled={props.currentState=="loading"} onClick={props.onSubmit} color="primary" site="lg"><FontAwesomeIcon icon={faCheck} /> Submit</Button>
                </Form>
                
                
            </CardBody>
            <CardFooter>
                {
                    props.currentState == 'firstrun' ?
                    <div>
                            Don't have an URL at hand? <Button color="success" size="sm" outline onClick={props.onExampleUrl}>Click Here</Button>{' '}
                            to input one.
                    </div>
                    : props.currentState == 'loading' ?
                        <div>
                            <Spinner size="sm" color="success" /> Retrieving your future career...
                            <Progress color="success" striped value={Math.trunc(props.progress)} />
                        </div>
                    : props.currentState == 'completed' &&
                        <div>
                            <Form>
                                <FormGroup row>
                                    <Col sm={2}>
                                        <label htmlFor="hnSearch">Search text</label>
                                    </Col>
                                    <Col sm={10}>
                                        <Input bsSize="sm" type="text" name="hnSearch" id="hnSearch"
                                            placeholder="e.g. junior"
                                            onChange={props.onSearchChange}
                                            value={props.searchText}
                                        />
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col sm={2}>
                                        <label htmlFor="hnRemote">Location</label>
                                    </Col>
                                    <Col sm={10}>
                                        <ButtonGroup>
                                            <Button size="sm" color="danger"  onClick={() => props.onLocationSelected("onsite")} outline={!props.selectedLocation.includes("onsite")}>ONSITE</Button>
                                            <Button size="sm" color="primary" onClick={() => props.onLocationSelected("remote")} outline={!props.selectedLocation.includes("remote")}>REMOTE</Button>
                                        </ButtonGroup>
                                        {' '}<Button size="sm" onClick={props.onResetLocation} outline>Reset</Button>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col sm={2}>
                                        <label htmlFor="hnTerms">Terms</label>
                                    </Col>
                                    <Col sm={10}>
                                        <div>
                                            {
                                                props.searchTrends.map((trend) => {
                                                    return <Button size="sm" color="success" key={"search"+trend} onClick={() => props.onTrendSelected(trend)} outline={!props.selectedTrends.includes(trend)}>{trend.toUpperCase()}</Button>
                                                })
                                            }
                                            
                                        </div>
                                        {' '}<Button size="sm" onClick={props.onResetTrends} outline>Reset</Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                            Showing {props.stats.filtered} job offer{props.stats.filtered!=1 && 's'} ({props.stats.total} total)
                        </div>
                }
            </CardFooter>
        </Card>
    )
}

export default SearchForm