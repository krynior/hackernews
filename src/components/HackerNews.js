import React from "react"
import Layout from "./Layout/Layout"
import {chunkArray, promiseSequential} from "../helpers/ChunkProcess"
import {debounce} from "throttle-debounce"
import JobParser from "../helpers/JobParser";

export default class HackerNews extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            posts: [],
            url:   '',
            alert: { show: false, text: ''},
            currentState: 'firstrun',
            progress: 0,
            progressMax: 1,
            searchText: '',
            searchTextDefer: '',
            searchTrends: [],
            selectedTrends: [],
            selectedLocation: []
        }
        this.requestPosts = this.requestPosts.bind(this)
        this.handleUrlChange = this.handleUrlChange.bind(this)
        this.showAlert = this.showAlert.bind(this)
        this.handleAlertDismiss = this.handleAlertDismiss.bind(this)
        this.handleExampleUrl = this.handleExampleUrl.bind(this)
        this.handleTrendSelected = this.handleTrendSelected.bind(this)
        this.handleSearchChanged = this.handleSearchChanged.bind(this)
        this.handleSearchDefered = debounce(1500, (newValue) => { this.setState({searchTextDefer: newValue})})
        this.handleLocationSelected = this.handleLocationSelected.bind(this)
        this.handleResetTrends = this.handleResetTrends.bind(this)
        this.handleResetLocation = this.handleResetLocation.bind(this)
        this.setCurrentState = this.setCurrentState.bind(this)
        this.setProgress = this.setProgress.bind(this)
        this.setProgressMax = this.setProgressMax.bind(this)
        this.setNewStories = this.setNewStories.bind(this)
    }

    render() {
        return (
            <Layout posts=         {this.state.posts}
                    onSubmit=      {this.requestPosts}
                    onUrlChange=   {this.handleUrlChange}
                    alert=         {this.state.alert}
                    onAlertDismiss={this.handleAlertDismiss}
                    currentState=  {this.state.currentState}
                    progress=      {this.state.progress}
                    onExampleUrl=  {this.handleExampleUrl}
                    url=           {this.state.url}
                    searchText=    {this.state.searchText}
                    searchTextDefer={this.state.searchTextDefer}
                    searchTrends=  {this.state.searchTrends}
                    selectedTrends={this.state.selectedTrends}
                    onTrendSelected={this.handleTrendSelected}
                    onSearchChange={this.handleSearchChanged}
                    selectedLocation={this.state.selectedLocation}
                    onLocationSelected={this.handleLocationSelected}
                    onResetLocation={this.handleResetLocation}
                    onResetTrends= {this.handleResetTrends}
             />
        )
    }

    handleUrlChange(event) {
        this.setState({url: event.target.value})
    }

    handleSearchChanged(event) {
        this.setState({searchText: event.target.value})
        this.handleSearchDefered(event.target.value)
    }

    handleResetLocation() {
        this.setState({selectedLocation: []})
    }

    handleResetTrends() {
        this.setState({selectedTrends: []})
    }

    showAlert(alertText) {
        this.setState({alert: {show: true, text: alertText}})
    }

    handleAlertDismiss() {
        this.setState({alert: {...this.state.alert, show: false}})
    }

    handleExampleUrl() {
        this.setState({url: 'https://news.ycombinator.com/item?id=19797594'})
    }

    setCurrentState(newState) {
        this.setState({currentState: newState})
    }

    setProgress(newProgress, increment) {
        const prog = increment==true ? this.state.progress + newProgress / this.state.progressMax * 100
                                     : newProgress / this.state.progressMax * 100
        this.setState({progress: prog})
    }

    setProgressMax(newProgressMax) {
        this.setState({progressMax: newProgressMax==0?1:newProgressMax})
    }

    setNewStories(newStories) {
        this.setState(
            {
                posts: newStories,
                selectedTrends: [],
                searchText: '',
                searchTextDefer: '',
                selectedLocation: [],
                searchTrends: [... new Set(newStories.reduce((prev, post) => { return prev.concat(post.trends) }, []))].sort()
            }
        )
    }

    toggleArray(arr, selected) {
        const index = arr.indexOf(selected)
        if (index < 0) {
          arr.push(selected)
        } else {
          arr.splice(index, 1)
        }
        return arr
    }

    handleTrendSelected(selected) {
        const arr = this.toggleArray(this.state.selectedTrends, selected)
        this.setState({ selectedTrends: [...arr] });
    }

    handleLocationSelected(selected) {
        const arr = this.toggleArray(this.state.selectedLocation, selected)
        this.setState({ selectedLocation: [...arr] });
    }

    requestPosts() {
        const self = this
        try {
            const url = new URL(this.state.url.trim());
            if (url.host != 'news.ycombinator.com'
            ||  url.protocol != 'https:'
            ||  url.pathname != '/item'
            ||  url.searchParams.get('id') == null) {
                throw new Error("Improper Hacker News story URL.")
            }
            const id = url.searchParams.get('id')

            fetch("https://hacker-news.firebaseio.com/v0/item/"+id+".json")
                .then((response) => {
                    return response.json()
                })
                .then((json) => {
                    if (json.by == "whoishiring") {
                        this.setCurrentState("loading")
                        this.setProgress(0, false)
                        const chunkSize = 100
                        const chunked = chunkArray(json.kids, chunkSize)
                        this.setProgressMax(chunked.length)
                        promiseSequential(chunked, (chunk) => {
                            self.setProgress(1, true)
                            return Promise.all(
                                chunk.map((item) => {
                                    return fetch("https://hacker-news.firebaseio.com/v0/item/"+item+".json")
                                            .then((response) => {
                                                return response.json()
                                            })
                                            .then((response) => {
                                                return response
                                            })
                                })
                            )
                        }).then((data) => {
                            this.setCurrentState("completed")
                            this.setNewStories(
                                    data.reduce((prev, cur) => prev.concat(cur),[])
                                        .filter((item) => {
                                            return !item.dead && !item.deleted
                                        })
                                        .map((item) => JobParser(item))
                            )
                        })
                    } else {
                        this.showAlert("Current story is not made by the user 'whoishiring'.")
                        this.setCurrentState("firstrun")
                    }
                })


        } catch(error) {
            this.showAlert(error.toString())
            this.setCurrentState("firstrun")
        }
    }
}
