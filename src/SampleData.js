const SampleData = [
  {
    "by" : "spacex_jobs",
    "id" : 18808169,
    "kids" : [ 18848662, 18814688 ],
    "parent" : 18807017,
    "text" : "SpaceX | Hawthorne, CA; Redmond, WA; Vandenberg, CA; Cape Canaveral, FL; McGregor, TX | Perm, On site, Full-Time | <a href=\"https:&#x2F;&#x2F;www.spacex.com&#x2F;\" rel=\"nofollow\">https:&#x2F;&#x2F;www.spacex.com&#x2F;</a><p>SpaceX designs, manufactures, and launches advanced rockets and spacecraft. The company was founded in 2002 under the belief that a future where humanity is out exploring the stars is fundamentally more exciting than one where we are not. Today SpaceX is actively revolutionizing space technology with the ultimate goal of enabling human life on Mars.<p>We currently have 300+ roles open across our various locations with the largest number of openings at our Hawthorne, CA headquarters. You can view all available jobs here:  <a href=\"https:&#x2F;&#x2F;www.spacex.com&#x2F;careers\" rel=\"nofollow\">https:&#x2F;&#x2F;www.spacex.com&#x2F;careers</a><p>Highlighted open roles:<p>- Software Engineer C++ (TS&#x2F;SCI Clearance): <a href=\"https:&#x2F;&#x2F;grnh.se&#x2F;6bb9b2a62\" rel=\"nofollow\">https:&#x2F;&#x2F;grnh.se&#x2F;6bb9b2a62</a><p>- Senior Software Engineer (Python): <a href=\"https:&#x2F;&#x2F;grnh.se&#x2F;0fe1cf412\" rel=\"nofollow\">https:&#x2F;&#x2F;grnh.se&#x2F;0fe1cf412</a><p>- GNC Mission Design Engineer: <a href=\"https:&#x2F;&#x2F;grnh.se&#x2F;b2ff294e2\" rel=\"nofollow\">https:&#x2F;&#x2F;grnh.se&#x2F;b2ff294e2</a><p>- Manager, Information Security Engineering: <a href=\"https:&#x2F;&#x2F;grnh.se&#x2F;ad1ea7cd2\" rel=\"nofollow\">https:&#x2F;&#x2F;grnh.se&#x2F;ad1ea7cd2</a><p>- Information Security Engineer:  <a href=\"https:&#x2F;&#x2F;grnh.se&#x2F;0c39363b2\" rel=\"nofollow\">https:&#x2F;&#x2F;grnh.se&#x2F;0c39363b2</a>",
    "time" : 1546451130,
    "type" : "comment"
  },
  {
    "by": "dan_qgiv",
    "id": 19910577,
    "parent": 19797594,
    "text": "Qgiv, Inc. | Software Engineer, PHP | Lakeland, FL | ONSITE | Qgiv.com<p>Our mission is to help people fulfill their passion to make a difference for others. Non-profit and faith-based organizations depend on Qgiv to power their fundraising through our industry leading software and growing suite of products and services. We absolutely love the work our clients do and we’re looking for someone who can help them achieve their mission through the Software Engineer role.<p>Apply Here:  <a href=\"https:&#x2F;&#x2F;qgiv.bamboohr.com&#x2F;jobs&#x2F;view.php?id=29\" rel=\"nofollow\">https:&#x2F;&#x2F;qgiv.bamboohr.com&#x2F;jobs&#x2F;view.php?id=29</a>",
    "time": 1557848553,
    "type": "comment"
  },
  {
    "by": "Signalrecruit",
    "id": 19799330,
    "parent": 19797594,
    "text": "Signal | SF or Remote (US only) | Full-Time | <a href=\"https:&#x2F;&#x2F;signal.org\" rel=\"nofollow\">https:&#x2F;&#x2F;signal.org</a><p>Signal is making private communication simple. As an Open Source project supported by grants and donations, Signal can put users first. There are no ads, no affiliate marketers, no creepy tracking. Just open technology for a fast, simple, and secure messaging experience. We design open protocols, develop Open Source software, and give it away for free.<p>To learn more about who we are, our engineering culture, and whether this is the right place for you, read our Key Values profile: <a href=\"https:&#x2F;&#x2F;www.keyvalues.com&#x2F;signal\" rel=\"nofollow\">https:&#x2F;&#x2F;www.keyvalues.com&#x2F;signal</a><p>Here are our open roles:<p>- Server Developer: <a href=\"https:&#x2F;&#x2F;jobs.lever.co&#x2F;signal&#x2F;2a5fee8b-5875-46d4-a41d-773a28a6b553\" rel=\"nofollow\">https:&#x2F;&#x2F;jobs.lever.co&#x2F;signal&#x2F;2a5fee8b-5875-46d4-a41d-773a28a...</a><p>- Android Developer: <a href=\"https:&#x2F;&#x2F;jobs.lever.co&#x2F;signal&#x2F;cc2a16be-b9aa-496e-ba2c-cf8ba3672267\" rel=\"nofollow\">https:&#x2F;&#x2F;jobs.lever.co&#x2F;signal&#x2F;cc2a16be-b9aa-496e-ba2c-cf8ba36...</a><p>- Desktop Developer (Web Developer): <a href=\"https:&#x2F;&#x2F;jobs.lever.co&#x2F;signal&#x2F;6cbff26c-290a-4e74-a56f-78e9783f3f90\" rel=\"nofollow\">https:&#x2F;&#x2F;jobs.lever.co&#x2F;signal&#x2F;6cbff26c-290a-4e74-a56f-78e9783...</a><p>Tech Stack: Android team: Java. Desktop team: Electron, web stack (js, css, etc.). Server team: Java, AWS, devops.<p>Please email us your resume or GitHub to: Workwithus@signal.org",
    "time": 1556730938,
    "type": "comment"
  }
];  
export default SampleData