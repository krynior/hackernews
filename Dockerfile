FROM node:lts-alpine AS build

WORKDIR /app
COPY . .

RUN npm install --save && npm run build

FROM nginx:stable-alpine

COPY --from=build /app/dist/ /var/www
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]